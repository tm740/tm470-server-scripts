from pymongo import MongoClient
import sys
import json

def loadData(file_to_process):
	with open(file_to_process) as f:
		data = json.load(f)
		return data

def ingestData(col, data_dict):
	success_count=0
	error_count=0
	
	#Iterate each car park in dataset
	for k,v in data_dict.items():
		try:
			#Insert document into database
			col.insert_one(data_dict[k])
			success_count+=1
		except Exception as e:
			print('Error ingesting data:', e)
			error_count+=1
	print('{} documents successfully ingested. Errors: {}'.format(str(success_count), str(error_count)))

if __name__ == "__main__":
	#Load encriched dataset
	file_to_process = sys.argv[1]
	data_dict = loadData(file_to_process)
	
	#Create MongoDB connection
	client = MongoClient('mongodb://testdbadmin:test123@127.0.0.1:27017/test')
	db = client['test']
	col = db.testcarparks
	
	#Ingest data
	ingestData(col, data_dict)