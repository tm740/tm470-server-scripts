#!/usr/bin/python3
from pymongo import MongoClient
import json

if __name__=='__main__':
	ids_list = []
	client = MongoClient('mongodb://testdbadmin:test123@127.0.0.1:27017/test')
	db = client['test']
	col = db.testcarparks
	data = col.find({}).limit(100000)
	for doc in data:
		if 'id' in doc:
			ids_list.append(doc['id'])
	
	print('{} IDs found'.format(len(ids_list)))
	
	out_str = ""
	for x in ids_list:
		out_str+=x+'\n'
	out_str = out_str[:-1]
	
	with open('ids_list_all.txt', 'w') as f:
		f.write(out_str)