import sys
import random
import os
import requests
import time
import datetime

def getIds(file_to_process):
    with open(file_to_process, 'r') as f:
        data = f.read().splitlines()
    return data

if __name__=='__main__':
    ids_file = sys.argv[1]
    interval=60
    
    ids = getIds(ids_file)
    print('Loaded {} Ids.'.format(str(len(ids))))
    
    count=0
    while True:
        count+=1
        for x in ids:
            cmd = "curl -i -X POST -k https://localhost:8443/api/cp/update -d \"id={}&capacity={}\"".format(x,random.randint(0,100))
            payload = {'id': x, 'capacity': random.randint(0,100)}
            r = requests.post('https://localhost:8443/api/cp/update', data=payload , verify=False)
            if not r.status_code == 200:
                print('Something went wrong. CODE: '+r.status_code)
        print(str(datetime.datetime.utcnow())+' Car parks updated. Iteration: '+str(count))
        time.sleep(interval)